﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpRavi
{
    enum PaymentProcessingType : int
    {
        None,
        CreditCard,
        DebitCard,
        NetBanking,
        CashOnDelivery
    }

    public class PaymentProcessing
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter the Cash Amount to Pay:");
            Decimal.TryParse(Console.ReadLine(), out decimal cashValue);
            if(cashValue==0)
            {
                Console.WriteLine("Enter the Cash Value");
                return;
            }

        PaymentData:
            Console.Write("Enter the Payment Processing Type:");
            string paymentProcess = Console.ReadLine();
            Enum.TryParse(paymentProcess,true, out PaymentProcessingType paymentProcessingType);

            switch (paymentProcessingType)
            {
                case PaymentProcessingType.CreditCard:
                    decimal interest = (0.08M * cashValue);
                    Console.WriteLine("Interest is {0}.", interest);
                    decimal totalAmount = cashValue + interest;
                    Console.WriteLine("Total Amount collected is {0}. Cash : {1} and Interest : {2}", totalAmount, cashValue, interest);
                    break;
                case PaymentProcessingType.CashOnDelivery:
                    Console.WriteLine("Total Amount collected is {0}.", cashValue);
                    break;
                case PaymentProcessingType.DebitCard:
                    interest = (0.01M * cashValue);
                    Console.WriteLine("Interest is {0}.", interest);
                    totalAmount = cashValue + interest;
                    Console.WriteLine("Total Amount collected is {0}. Cash : {1} and Interest : {2}", totalAmount, cashValue, interest);
                    break;
                case PaymentProcessingType.NetBanking:
                    interest = (0.05M * cashValue);
                    Console.WriteLine("Interest is {0}.", interest);
                    totalAmount = cashValue + interest;
                    Console.WriteLine("Total Amount collected is {0}. Cash : {1} and Interest : {2}", totalAmount, cashValue, interest);
                    break;
                default:
                    Console.WriteLine("Select a Proper Payment Value");
                    goto PaymentData;
            }
        }
    }
}
