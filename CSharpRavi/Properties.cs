﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpRavi
{
    class Properties
    {
        public Properties(string firstName)
        {
            _firstName = firstName;
        }
        private int _a;
        public int Sample
        {
            get { return _a; }
            set { _a = value; }
        }

        private string _firstName;
        public string FirstName { get { return _firstName; } }

        private string _lastName;

        public string LastName
        {
            get {
                if (_lastName.Length == 4)
                    return "";
                return _lastName.ToLower();
            }
            set { _lastName = value; }
        }

        public static void Main(string[] args)
        {
            Properties properties = new Properties("Revi");
            properties.LastName = "Ravis";
            Console.WriteLine($"{properties.FirstName} {properties.LastName}");
        }
    }
}
