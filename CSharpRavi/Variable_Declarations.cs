﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpRavi
{
    class Variable_Declaration
    { /*Variable Declaration
            * <access specifier> <data type> <variable name>
            */
        const string x = "a";
        static int a;
        static long b;
        static float c;
        static double d;
        static decimal Dec;

        static void Main(string[] args)
        {

            string strData = args[0];
            a = int.MaxValue;
            b = a;
            c = float.MaxValue;
            d = c;
            Dec = Decimal.MaxValue;

            Console.WriteLine(strData);
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);
            Console.WriteLine(Dec);

            a = unchecked((int)long.MaxValue);
            c = (float)double.MaxValue;
            c = (float)Decimal.MaxValue;
            d = (double)Decimal.MaxValue;
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);
            Console.WriteLine(Dec);
        }
    }
}
